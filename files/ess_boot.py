#!/usr/bin/env python2.7

"""Python script to execute all hostname specific executables in a common
script directory.

Runs all scripts in $scriptdir/$hostname that are marked as executable. Scripts
may be stored in any directory hierarchy. The default value of scriptdir is
/opt/startup/boot.
"""

from __future__ import print_function
import os
import sys
import argparse
import socket
import subprocess
import shlex


def main(arguments):
    """Main function"""
    parser = argparse.ArgumentParser(
        description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter
    )
    parser.add_argument("action", help="Action to forward to script")
    parser.add_argument(
        "--script-dir", help="Directory with scripts", default="/opt/startup/boot"
    )
    parser.add_argument(
        "-o",
        "--outfile",
        help="Output file",
        default=sys.stdout,
        type=argparse.FileType("w"),
    )

    args = parser.parse_args(arguments)
    hostname = socket.gethostname()
    out = args.outfile

    scriptdir = os.path.join(args.script_dir, hostname)
    if not os.path.isdir(scriptdir):
        out.write("[II] Directory doesn't exist: {}\n".format(scriptdir))
        return 0
    for (root, _, files) in os.walk(scriptdir):
        for name in files:
            if os.access(os.path.join(root, name), os.X_OK):
                out.write("[II] Running {} {}\n".format(name, args.action))
                try:
                    status = subprocess.call(
                        shlex.split(
                            "{} {}".format(os.path.join(root, name), args.action)
                        )
                    )
                    if status != 0:
                        out.write("[WW] Process returned error code {}".format(status))
                except OSError:
                    out.write(
                        "[WW] Failed to execute {}\n".format(os.path.join(root, name))
                    )
            else:
                out.write("[II] Skipping {}, it not executable\n".format(name))


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
